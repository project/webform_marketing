Welcome to the Webform marketing module.

-- SUMMARY --
With the Webform marketing module you are able to fetch url parameters and external site refer in webform fields with the 'Webform marketing' field type. The data is stored in a cookie, so you can have a different landing page then your webform.

The module is maintained by ADCI Solutions and sponsored by Roods.

-- DEPENDENCIES --

* Webform 7.x-3.x (tested with 7.x-3.19), available at https://drupal.org/project/webform

-- INSTALLATION --

Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION --

Please see your Drupal installation help page admin/help/webform_marketing for usage instructions.

HINT: Please respect your local cookie law. If you need to inform the visitor by a privacy policy, please do so.
